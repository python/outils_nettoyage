# -*- coding: utf-8 -*-
 
from tkinter import *
import webbrowser

## Declaration des URLs
def dir_adw():
	webbrowser.open('https://toolslib.net/downloads/viewdownload/1-adwcleaner/')
def dir_malw():
	webbrowser.open('https://fr.malwarebytes.com//')
def dir_jrt():
	webbrowser.open('https://fr.malwarebytes.com/junkwareremovaltool/')
def dir_wroot():
	webbrowser.open('https://toolslib.net/downloads/viewdownload/565-webrootdebug/')
def dir_zhp():
	webbrowser.open('https://www.nicolascoolman.com/fr/download/zhpcleaner')
def dir_cc():
	webbrowser.open('https://www.ccleaner.com/fr-fr/ccleaner/download')
def dir_zhps():
	webbrowser.open('https://www.nicolascoolman.com/fr/download/zhpdiag/')
	webbrowser.open('https://www.nicolascoolman.com/fr/download/zhplite/')
	webbrowser.open('https://www.nicolascoolman.com/fr/download/zhpfix')

## Initialisation de la fenetre Tkinter
w = Tk()
w.title("Outils de Nettoyage WINDOWS10")
w['bg']='grey'


## Initialisation de la grille
empty1=Label(w, text="", bg='grey')
empty1.grid(row=0, column=0)
empty2a=Label(w, text="", bg='grey')
empty2a.grid(row=2, column=0)
empty2b=Label(w, text="", bg='grey')
empty2b.grid(row=3, column=0)
empty3=Label(w, text="", bg='grey')
empty3.grid(row=4, column=0)
empty3b=Label(w, text="", bg='grey')
empty3b.grid(row=5, column=0)
empty4=Label(w, text="", bg='grey')
empty4.grid(row=6, column=0)
empty4b=Label(w, text="", bg='grey')
empty4b.grid(row=7, column=0)
empty5=Label(w, text="", bg='grey')
empty5.grid(row=8, column=0)

## Disposition du texte
lr1=Label(w, text="Minimum à passer", bg='grey', fg='white')
lr1.grid(row=1, column=1)
lr2=Label(w, text="Optionnel / Obselète", bg='grey', fg='white')
lr2.grid(row=4, column=1)
lr3=Label(w, text="Puissant /!\ ", bg='grey', fg='white')
lr3.grid(row=7, column=1)

## Disposition des boutons
b1=Button(w, text="ADWCleaner", command=dir_adw, relief=GROOVE)
b1.grid(row=1, column=3)
b2=Button(w, text="MalwareBytes", command=dir_malw, relief=GROOVE)
b2.grid(row=1, column=5)
b3=Button(w, text="JunkwareRemovalTool", command=dir_jrt, relief=GROOVE)
b3.grid(row=4, column=3, padx=15)
b4=Button(w, text="Webroot DE-BUG", command=dir_wroot, relief=GROOVE)
b4.grid(row=4, column=5)
b5=Button(w, text="CCleaner", command=dir_cc, relief=GROOVE)
b5.grid(row=4, column=7)
b6=Button(w, text="ZHP Cleaner", command=dir_zhp, relief=GROOVE)
b6.grid(row=7, column=3, padx=15)
b7=Button(w, text="ZHP Diag + Lite + Fix", command=dir_zhps, relief=GROOVE)
b7.grid(row=7, column=5)
b8=Button(w, text="Quitter", command=w.destroy, relief=FLAT, bg='blue')
b8.grid(row=9, column=8, padx=10, pady=10)

w.mainloop()
